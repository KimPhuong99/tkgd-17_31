import React from 'react';
import './App.scss';
import 'antd/dist/antd.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from './pages/Home';
import AddressForm from './pages/AddressForm';
import PaymentMethods from './pages/PaymentMethods';
import Checkout from './pages/Checkout';
import CartTable from './pages/CartTable';
import ImageIdea from './pages/ImageIdea';
import Login from './pages/Login';
import Logout from './pages/Logout';

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/address-form' component={AddressForm} />
        <Route path='/payment-methods' component={PaymentMethods} />
        <Route path='/checkout' component={Checkout} />
        <Route path='/cart-table' component={CartTable} />
        <Route path='/image-idea' component={ImageIdea} />
        <Route path='/home' component={Home} />
        <Route path='/logout' component={Logout} />
        <Route path='/login' component={Login} />
        <Route path='/' component={Home} />
      </Switch>
    </Router>
  );
}

export default App;
