import React from "react";

import "./CardPicture.scss";

import AddPicture from "../AddPicture";

function CardPicture(props) {
  return (
    <div className="card-picture">
      <div className="layer"></div>
      <AddPicture source={props.source} />
      <img src={props.source} alt="pic" width="100%" height="500px" />
      <div className="card-content">
        {props.content}
        <br />
        {props.author}
      </div>
    </div>
  );
}

export default CardPicture;
