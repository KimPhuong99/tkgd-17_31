import React from "react";
import Logo from "./images/logo.png";
import UserAvatar from "./images/user-avatar.png";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Navbar, Form, FormControl, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faShoppingCart,
  faHeart,
  faBell,
} from "@fortawesome/free-solid-svg-icons";

import authenticationService from "../../services/authentication";

const CustomNavbar = () => {
  return (
    <>
      <Navbar
        className="d-flex justify-content-between"
        expand="lg"
        bg="white"
        variant="white"
        sticky="top"
      >
        <Navbar.Brand href="/">
          <img src={Logo} className="logo" alt="logo" />
        </Navbar.Brand>

        <div className="form-search">
          <Form inline>
            <Button className="btn-search-header btn">
              <FontAwesomeIcon icon={faSearch} />
            </Button>
            <FormControl
              type="text"
              placeholder="Tìm kiếm sản phẩm, ý tưởng"
              className="mr-sm-2"
            />
          </Form>
        </div>
        <div className="d-flex justify-content-between">
          <div className="tools">
            <a href="/cart-table">
              <FontAwesomeIcon icon={faShoppingCart} />
            </a>
            <FontAwesomeIcon icon={faHeart} />
            <FontAwesomeIcon icon={faBell} />
          </div>
          {authenticationService.isLogin() && (
            <div className="infoBtn">
              <UncontrolledDropdown>
                <DropdownToggle>
                  <img
                    src={UserAvatar}
                    className="avatar"
                    roundedCircle
                    alt="user"
                  />
                  &nbsp; Nguyễn Hoài Bảo
                </DropdownToggle>
                <DropdownMenu right color="#117182">
                  <DropdownItem>Cập nhật profile</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    <a href="/logout">Đăng xuất</a>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </div>
          )}
          {authenticationService.isLogout() && (
            <div
              className="infoBtn"
              style={{ color: "#117182", padding: "10px" }}
            >
              <a href="/login">Đăng nhập</a>
              &nbsp; / &nbsp;
              <a href="/register">Đăng ký</a>
            </div>
          )}
        </div>
      </Navbar>
      <div className="divider"></div>
    </>
  );
};

export default CustomNavbar;
