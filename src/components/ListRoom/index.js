import React, { useState } from "react";
import Room from "./Room";
import "./index.css";
import room1 from "./images/room1.jpg";
import room2 from "./images/room2.jpg";
import room3 from "./images/room3.jpg";
import room4 from "./images/room4.jpg";
import room5 from "./images/room5.jpg";
import room6 from "./images/room6.jpg";
import room7 from "./images/room7.jpg";
import room8 from "./images/room8.jpg";
import room9 from "./images/room9.jpg";
import room10 from "./images/room10.jpg";
import room11 from "./images/room11.jpg";
import room12 from "./images/room12.jpg";
import room13 from "./images/room13.jpg";
import room14 from "./images/room14.jpg";

const rooms = [
  {
    backgroundImage: room1,
    label: "Phòng bếp",
  },
  {
    backgroundImage: room2,
    label: "Phòng ngủ",
  },
  {
    backgroundImage: room3,
    label: "Phòng ăn",
  },
  {
    backgroundImage: room4,
    label: "Phòng tắm",
  },
  {
    backgroundImage: room5,
    label: "Phòng làm việc",
  },
  {
    backgroundImage: room6,
    label: "Phòng Gym",
  },
  {
    backgroundImage: room7,
    label: "Bên ngoài",
  },
  {
    backgroundImage: room8,
    label: "Phòng trẻ em",
  },
  {
    backgroundImage: room9,
    label: "Nhà kho",
  },
  {
    backgroundImage: room10,
    label: "Nhà riêng",
  },
  {
    backgroundImage: room11,
    label: "Cầu thang",
  },
  {
    backgroundImage: room12,
    label: "Hầm rượu",
  },
  {
    backgroundImage: room13,
    label: "Phòng tắm hơi",
  },
  {
    backgroundImage: room14,
    label: "Phòng khách",
  },
  {
    backgroundImage: room10,
    label: "Phòng xông hơi",
  },
];

function ListRoom() {
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(7);
  const changeStart = () => {
    start != 0 ? setStart(start - 5) : setStart(start);
  };
  const changeEnd = () => {
    start + 5 != 15 ? setStart(start + 5) : setStart(start);
  };
  const renderListRoom1 = () => {
    return (
      rooms &&
      rooms.slice(start, 15).map((room, index) => {
        return (
          <Room
            id={index}
            backgroundImage={room.backgroundImage}
            label={room.label}
          />
        );
      })
    );
  };
  return (
    <div>
      <div class="row">
        <div
          class="MultiCarousel"
          data-items="1,3,5,6"
          data-slide="1"
          id="MultiCarousel"
          data-interval="1000"
        >
          <div class="MultiCarousel-inner">
            <div className="d-flex justify-content-center">
              {renderListRoom1()}
            </div>
          </div>
          <button
            class="btn button-change leftLst"
            onClick={changeStart}
            style={{ marginLeft: "16px" }}
          >
            &#171;
          </button>
          <button class="btn rightLst button-change" onClick={changeEnd}>
            &#187;
          </button>
        </div>
      </div>
      <br />
    </div>
  );
}

export default ListRoom;
