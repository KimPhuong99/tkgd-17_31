import React from "react";
import '../index.css'
function Room({ backgroundImage, label }) {
  return (
   
      <div
        className="shadow card"
        style={{ width: "11.5rem", margin: "5px", height: "13rem"}}
      > <a className="linkRoom">
        <img
          className="card-img-top"
          src={backgroundImage}
          alt="Card image cap"
          style={{ height: "8rem" }}
        />
        <div className="card-body">
          <h5 className="card-text">{label}</h5>
        </div></a>
      </div>
    
  );
}

export default Room;
