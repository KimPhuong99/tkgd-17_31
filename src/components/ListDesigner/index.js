import React from 'react';

import DesignerRow from './DesignerRow';

import designer1 from './images/designer1.jpg';
import designer2 from './images/designer2.jpg';
import designer3 from './images/designer3.jpg';
import designer4 from './images/designer4.jpg';
import designer5 from './images/designer5.jpg';
import designer6 from './images/designer6.jpg';

const designers = [
  {
    title: 'John Nguyễn',
    subtitle: 'Thiết kế phòng ngủ',
    backgroundImage: designer1
  },
  {
    title: 'Nguyễn Hà Linh',
    subtitle: 'Thiết kế phòng khách',
    backgroundImage: designer2
  },
  {
    title: 'LyLy Nguyễn',
    subtitle: 'Thiết kế phòng ăn',
    backgroundImage: designer3
  },
  {
    title: 'Thảo Trương',
    subtitle: 'Thiết kế phòng ngủ',
    backgroundImage: designer4
  },
  {
    title: 'Bùi Đức Hải',
    subtitle: 'Thiết kế phòng Gym',
    backgroundImage: designer5
  },
  {
    title: 'Đỗ Anh Tuấn',
    subtitle: 'Thiết kế phòng ngủ',
    backgroundImage: designer6
  }
];

function ListDesigner() {
  return(
    <div>
      <DesignerRow items={designers.slice(0, 3)} />
      <br />
      <DesignerRow items={designers.slice(3)} />
    </div>
  );
}

export default ListDesigner;