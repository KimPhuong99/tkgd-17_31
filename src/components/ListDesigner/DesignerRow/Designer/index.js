import React from 'react';
import './index.scss';
function Designer({title, subtitle, backgroundImage}) {
  return (
    <a href="#">
      <div style={{ marginRight: "30px" }} className="designer">
        <img
          style={{ margin: "0px auto" }}
          src={backgroundImage}
          width="300px"
          height="200px"
          alt="designer"
        />
        <div
          style={{
            backgroundColor: "#2b7a78",
            padding: ".5rem 0rem .1rem",
          }}
        >
          <div className="font01" style={{ color: "#feffff" }}>
            {title}
          </div>
          <div className="font02" style={{ color: "#feffff" }}>
            {subtitle}
          </div>
        </div>
      </div>
    </a>
  );
}

export default Designer;