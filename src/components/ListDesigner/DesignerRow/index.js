import React from 'react';

import Designer from './Designer';

function DesignerRow({items}) {
  const renderDesignerRow = () => {
    return items && items.map((item, index) => {
      return(
        <Designer id={index} title={item.title} 
        subtitle={item.subtitle}  backgroundImage={item.backgroundImage} />
      );
    });
  };

  return(
    <div className="text-left">
      <div className="d-flex justify-content-center">
      {renderDesignerRow()}
      </div>
    </div>
  );
}

export default DesignerRow;