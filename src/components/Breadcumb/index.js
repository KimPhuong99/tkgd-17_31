import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleRight
} from "@fortawesome/free-solid-svg-icons";
const Breadcumb = ({items})=>{
    return (
      <>
        <div class="level">
          {
            items && items.map((item, index) => {
              if(!item.check) {
                return <a class="link active" href={item.link}>
                {item.text} <FontAwesomeIcon icon={faAngleRight} />
                </a>
              }
              return <a class="link " href={item.link}>
                {item.text}
              </a>
            })
          }
        </div>
      </>
    );
}

export default Breadcumb;