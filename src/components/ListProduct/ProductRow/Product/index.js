import React from 'react';
import '../index.css';
function Product({backgroundImage}) {
  return(
    <a href="#" style={{"marginLeft":"-50px","marginRight":"40px"}}>
      <div className="product shadow p-3 mb-5 bg-white rounded ml-5" >
        <img  className = "pro" style={{margin: '0px auto'}} 
          src={backgroundImage} 
          alt="product" />
          <br />
          <h5 style={{color: 'black'}}>
            Product Name
          </h5>
          <div>2.500.000đ</div>
      </div>
    </a>
  );
}

export default Product;