import React, { useEffect } from "react";
import "./index.css";
import Product from "./Product";
import WOW from "wowjs";

function ProductRow({ title, backgroundImages }) {
  useEffect(() => {
    new WOW.WOW({
      live: false,
    }).init();
  }, []);

  const renderProductRow = () => {
    return (
      backgroundImages &&
      backgroundImages.map((backgroundImage, index) => {
        return <Product id={index} backgroundImage={backgroundImage} />;
      })
    );
  };

  return (
    <>
      <div className="wow fadeInRight center" data-wow-delay="0.3s">
        <div class="row">
          <h3 class="font19">{title}</h3>
          <div
            class="MultiCarousel"
            data-items="1,3,5,6"
            data-slide="1"
            id="MultiCarousel"
            data-interval="1000"
          >
            <div class="MultiCarousel-inner">
              <div className="d-flex justify-content-center">
                {renderProductRow()}
              </div>
            </div>
            <button
              class="btn button-change leftLst"
              style={{ fontSize: "2rem" }}
            >
              &#171;
            </button>
            <button
              class="btn rightLst button-change"
              style={{ fontSize: "2rem" }}
            >
              &#187;
            </button>
          </div>
        </div>
      </div>
      <div className="wow fadeInUp center" data-wow-delay="0.6s">
        <div class="d-flex flex-row-reverse bd-highlight">
          <a href="#">Xem thêm</a>
        </div>
      </div>
    </>
  );
}

export default ProductRow;
