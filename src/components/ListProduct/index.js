import React, { useEffect } from "react";

import ProductRow from "./ProductRow";
import WOW from "wowjs";
import product1 from "./images/product1.jpg";
import product2 from "./images/product2.jpg";
import product3 from "./images/product3.jpg";
import product4 from "./images/product4.jpg";
import product5 from "./images/product5.jpg";
import product6 from "./images/product6.jpg";
import product7 from "./images/product7.jpg";
import product8 from "./images/product8.jpg";
import product9 from "./images/product9.jpg";
import product10 from "./images/product10.jpg";
import product11 from "./images/product11.png";
import product12 from "./images/product12.jpg";
import product13 from "./images/product13.jpg";
import product14 from "./images/product14.jpg";

const productImages = [
  product1,
  product2,
  product3,
  product4,
  product5,
  product6,
  product7,
  product8,
  product9,
  product10,
  product11,
  product12,
  product13,
  product14,
];

function ListProduct() {
  useEffect(() => {
    new WOW.WOW({
      live: false,
    }).init();
  }, []);
  return (
    <div>
      <ProductRow
        title="Sản phẩm được mua nhiều nhất"
        backgroundImages={productImages.slice(0, 5)}
      />
      {/* </div> */}
      <br />
      <ProductRow
        title="Sản phẩm mới"
        backgroundImages={productImages.slice(5, 10)}
      />
      <br />
      <ProductRow
        title="Sản phẩm đề xuất cho bạn"
        backgroundImages={productImages.slice(10, 14)}
      />
    </div>
  );
}

export default ListProduct;
