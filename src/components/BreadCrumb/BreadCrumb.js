import React from "react";
import PropTypes from "prop-types";
import { Breadcrumb } from "antd";
import './BreadCrumb.scss';
BreadCrumb.propTypes = {};

function BreadCrumb(props) {
  return (
    <div>
      <Breadcrumb separator=">" className="breadCrumb">
        <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
        <Breadcrumb.Item href="">Ý tưởng</Breadcrumb.Item>
        <Breadcrumb.Item href="">Hình ảnh</Breadcrumb.Item>
      </Breadcrumb>
    </div>
  );
}

export default BreadCrumb;
