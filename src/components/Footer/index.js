import React from "react";
import "./MyFooter.scss";
Footer.propTypes = {};

function Footer(props) {
  return (
    <footer class="footer text-center text-lg-start">
      <div class="container p-4">
        <div class="row">
          <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
            <h5 class="text-uppercase text-light">THÔNG TIN LIÊN LẠC</h5>

            <div class="item d-flex flex-column align-items-start info">
              <p>
                Trụ sở: Ngô Đức Kế, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh,
                Việt Nam
              </p>
              <p>
                Hotline: <span>0906 264 268</span>
              </p>
              <p>
                Tư vấn thiết kế:<span> 090 636 2998</span>
              </p>

              <p>
                Email:{" "}
                <a href="mailto:contact@idesign.com.vn">
                  <span>contact@idesign.com.vn</span>
                </a>
              </p>

              <p>
                Website:{" "}
                <a href="http://idesign.com.vn">
                  <span>idesign.com.vn</span>
                </a>
              </p>

              <p>Giấy CNĐKKD :&nbsp;0104257842 do sở KHĐT TP HN cấp</p>

              <p>Ngày: 17/11/2009 - Sửa đổi lần 8 ngày:18/5/2016</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase text-light">HỢP TÁC</h5>

            <ul class="list-unstyled  d-flex flex-column align-items-start">
              <li>
                <a href="#!" h3>
                  Cá Nhân
                </a>
              </li>
              <li>
                <a href="#!" h3>
                  Cửa Hàng
                </a>
              </li>
              <li>
                <a href="#!" h3>
                  Nhà Thiết Kế
                </a>
              </li>
              <li>
                <a href="#!" h3>
                  Doanh Nghiệp
                </a>
              </li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase text-light">SẢN PHẨM</h5>

            <ul class="list-unstyled d-flex flex-column align-items-start ">
              <li>
                <a href="#!" h3>
                  Nội Thất
                </a>
              </li>
              <li>
                <a href="#!" h3>
                  Thiết kế
                </a>
              </li>
              <li>
                <a href="#!" h3>
                  Xây Dựng
                </a>
              </li>
              <li>
                <a href="#!" h3>
                  Trang Trí
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="text-center p-3 w-100 myfoot">
        © 2020 Copyright
        <a class="mx-2" href="https://mdbootstrap.com/">
          Interior Design Inc.
        </a>
      </div>
    </footer>
  );
}

export default Footer;
