import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import {
  FaCartPlus,
  FaLightbulb,
  FaGlobeAfrica,
  FaAddressBook
} from 'react-icons/fa';

function Menu() {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return(
    <div>
      <Navbar light expand="md" >
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mx-auto nav-custom" navbar>
            <NavItem>
              <NavLink href="/shopping"><FaCartPlus /> Mua sắm</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav>
                <FaLightbulb /> Ý tưởng
              </DropdownToggle>
              <DropdownMenu right color="#117182">
                <DropdownItem>
                  <a href="/image-idea">Hình ảnh</a>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  Video
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem>
              <NavLink href="/trend"><FaGlobeAfrica /> Xu hướng</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/designer"><FaAddressBook /> Nhà thiết kế</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Menu;