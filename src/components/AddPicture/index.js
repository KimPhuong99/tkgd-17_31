import React, { useEffect, useState } from "react";
import { Modal, Button, Radio } from "antd";
import { AiFillHeart } from "react-icons/ai";
import { Row, Col, Input } from "antd";
import pic6 from "./images/pic6.jfif";
import pic7 from "./images/pic7.jfif";
import pic8 from "./images/pic8.jfif";

import "./AddPicture.scss";
const AddPicture = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [finish, setFinish] = useState(false);
  const [loading, setLoading] = useState(false);
  const [newAlbum, setNewAlbum] = useState("");
  const [note, setNote] = useState("");
  const [bst, setBst] = useState([
    {
      id: "1",
      name: "Yêu thích",
      src: [
        { pic: pic6, note: "Love" },
        { pic: pic7, note: "You" },
      ],
    },
    {
      id: "2",
      name: "Phòng khách yêu thích",
      src: [{ pic: pic8, note: "Living room" }],
    },
  ]);
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    bst[value - 1].src.push(props.source);
    if (newAlbum != "") {
      setTimeout(
        () =>
          setBst([
            ...bst,
            {
              id: bst.length + 1,
              name: newAlbum,
              src: [{ pic: props.source, note: note }],
            },
          ]),
        2000
      );
    }

    setLoading(true);
    setTimeout(() => setFinish(true), 2000);
    setTimeout(() => setIsModalVisible(false), 1800);
    setTimeout(
      () =>
        Modal.success({
          title: "Đã lưu thành công vào",
          content: (
            <div className="add-success">
              <img
                src={props.source}
                height="120px"
                width="150px"
                style={{ borderRadius: "5px" }}
              />
              <span className="name-album-added">
                {newAlbum !== "" ? newAlbum : bst[value - 1].name}
              </span>
            </div>
          ),
        }),
      2000
    );
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleNewAlbum = (e) => {
    setNewAlbum(e.target.value);
  };

  const handleNote = (e) => {
    setNote(e.target.value);
  };

  const [value, setValue] = React.useState("1");

  const onChange = (e) => {
    console.log("radio checked", e.target.value);
    setValue(e.target.value);
  };

  console.log(newAlbum);
  console.log(finish);

  return (
    <>
      <a className="card-icon" onClick={showModal}>
        <AiFillHeart className="card-heart" />
      </a>
      <Modal
        title={finish ? "Thành công" : "Thêm vào bộ sưu tập"}
        footer={null}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        wrapClassName="modal-add-album"
        destroyOnClose={true}
        width={800}
        afterClose={() => {
          setFinish(false);
          setIsModalVisible(false);
          setLoading(false);
          setNewAlbum("");
        }}
      >
        <Row gutter={16}>
          <Col span={12}>
            <Radio.Group onChange={onChange} value={value}>
              {bst.map((album, index) => (
                <div key={index} className="album">
                  <Radio value={album.id}>
                    <img src={album.src[0].pic} height="90px" width="110px" />
                    <span className="name-album">{album.name}</span>
                  </Radio>
                </div>
              ))}
            </Radio.Group>
            <Input
              onChange={handleNewAlbum}
              placeholder="+ Thêm bộ sưu tập mới"
              style={{ width: "85%", marginLeft: "23px" }}
              onPressEnter={(e) => {
                e.preventDefault();
              }}
            />
          </Col>

          <Col span={12}>
            <img src={props.source} alt="image" className="image" />

            <Input.TextArea
              placeholder="Ghi chú cho hình ảnh"
              onChange={handleNote}
              style={{ width: "100%", marginTop: "20px" }}
            />

            <div className="btnModal">
              <Button className="btn-album" onClick={handleCancel}>
                Thoát
              </Button>
              <Button
                type="primary"
                className="btn-album-primary"
                onClick={handleOk}
                loading={loading && true}
              >
                Lưu
              </Button>
            </div>
          </Col>
        </Row>
      </Modal>
    </>
  );
};
export default AddPicture;
