import React from "react";
import "./index.css";
function Video() {
  return (
    <div style={{ marginLeft: "10px" }}>
      <div class="row">
        <div class="col-7">
          <div class="d-flex flex-column bd-highlight mb-3">
            <a href="#">
              <img
                className="img1"
                src="https://drawings.archicgi.com/wp-content/uploads/2020/03/3d-models-in-revit-ready-families-for-interior-design-View01-e1586857212352.jpg"
              ></img>
            </a>
            <h3 class="font1">10 KIỂU THIẾT KẾ NỘI THẤT 2020</h3>
            <span class="font2">John Nguyễn</span>
          </div>
        </div>
        <div class="col">
          <div class="row">
            <div class="col">
              <a href="#">
                <img
                  className="img2"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM9m2Lh76OEan87miK_o_nKrrScT7Dfn2Lg&usqp=CAU"
                ></img>
              </a>
            </div>
            <div class="col" style={{ textAlign: "start" }}>
              <h5 class="font11">Phong cách thiết kế hiện đại</h5>
              <span class="font22">Nguyễn Như Anh</span>
            </div>
          </div>
          <div class="row" style={{ marginTop: "11px" }}>
            <div class="col">
              <a href="#">
                <img
                  className="img2"
                  src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/patio-ideas-designed-by-emily-henderson-design-photo-by-sara-tramp-ligorria-8-1587676367.jpg"
                ></img>
              </a>
            </div>
            <div class="col" style={{ textAlign: "start" }}>
              <h5 class="font11">Nhà đẹp 2020</h5>
              <span class="font22">Lê Thiên hà</span>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex flex-row-reverse bd-highlight">
        <a href="#">Xem thêm</a>
      </div>
    </div>
  );
}
export default Video;
