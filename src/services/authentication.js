export default {
  getUser: function() {
    return localStorage.getItem('user');
  },
  login: function(resObject) {
    const {user} = resObject;
    localStorage.setItem('user', user);
  },
  logout: function() {
    localStorage.removeItem('user');
  },
  isLogin: function() {
    return localStorage.getItem('user') ? true : false;
  },
  isLogout: function() {
    return localStorage.getItem('user') ? false : true;
  }
};