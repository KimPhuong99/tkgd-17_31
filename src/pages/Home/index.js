import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import ListProduct from "../../components/ListProduct";
import ListRoom from "../../components/ListRoom";
import ListDesigner from "../../components/ListDesigner";
import Menu from "../../components/Menu";
import Slider from "../../components/Slider";
import Video from "../../components/listVideo";
import { ArrowUpOutlined } from "@ant-design/icons";
import { BackTop } from "antd";
function Home() {
  return (
    <div>
      <Header />
      <Menu />
      <br />
      <div className="body">
        <Slider />
        <br />
        <div className="d-flex text-center">
          <hr
            style={{
              width: "30%",
              borderTop: "2px solid  #3AAFA9",
              textAlign: "left",
              marginLeft: 0,
            }}
          />
          <h3 style={{ color: "#3AAFA9" }}>Ý tưởng</h3>
          <hr
            style={{
              width: "30%",
              borderTop: "2px solid  #3AAFA9",
              textAlign: "right",
              marginRight: 0,
            }}
          />
        </div>
        <br />
        <ListRoom />
        <Video />
        <br />
        <div className="d-flex text-center">
          <hr
            style={{
              width: "30%",
              borderTop: "2px solid  #3AAFA9",
              textAlign: "left",
              marginLeft: 0,
            }}
          />
          <h3 style={{ color: "#3AAFA9" }}>Sản phẩm</h3>
          <hr
            style={{
              width: "30%",
              borderTop: "2px solid  #3AAFA9",
              textAlign: "right",
              marginRight: 0,
            }}
          />
        </div>
        <br />
        <ListProduct />
        <br />
        <div className="d-flex text-center">
          <hr
            style={{
              width: "30%",
              borderTop: "2px solid  #3AAFA9",
              textAlign: "left",
              marginLeft: 0,
            }}
          />
          <h3 style={{ color: "#3AAFA9" }}>Nhà thiết kế</h3>
          <hr
            style={{
              width: "30%",
              borderTop: "2px solid  #3AAFA9",
              textAlign: "right",
              marginRight: 0,
            }}
          />
        </div>
        <br />
        <ListDesigner />
        <br />
      </div>
      <BackTop>
        <div className="btn-Top">
          <ArrowUpOutlined className="icon-Top" />
        </div>
      </BackTop>
      <Footer />
    </div>
  );
}

export default Home;
