import React from "react";
import { Button, Table, Container } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CurrencyFormat from "react-currency-format";
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Menu from '../../components/Menu';
import Breadcumb from '../../components/Breadcumb';
import Img1 from "./images/Rectangle5.png";
import Img2 from "./images/ghe.png";
import Img3 from "./images/erectangle_5.png";
import Swal from "sweetalert";
import {
  faLongArrowAltRight
} from "@fortawesome/free-solid-svg-icons";
import Step3 from "./images/proccess-bar.svg";

const Checkout = () => {
  function handleClick(e) {
    Swal({
      title: "Đặt hàng thành công!",
      icon: "success", 
    });
    setTimeout(function () {
     window.open("http://localhost:3000", "_self");
    }, 5000);
     
  }

  const items = [
    {
      text: 'Trang chủ',
      link: '/',
      check: false
    },
    {
      text: 'Trang giỏ hàng',
      link: '/cart-table',
      check: false
    },
    {
      text: 'Thanh toán giỏ hàng',
      link: '#',
      check: true
    }
  ];

  const products = [
    {
      id: 1,
      name: "Bồn tắm Oval",
      code: "RU-8329",
      shop: "Euro King",
      price: 1200000,
      quantity: 1,
      isChecked: true,
      image: Img1,
    },
    {
      id: 2,
      name: "Ghế phòng ăn",
      code: "Velvet MQ-2847",
      shop: "Winchar.vn",
      price: 250000,
      quantity: 2,
      isChecked: true,
      image: Img2,
    },
    {
      id: 3,
      name: "Đèn trần thả kim cương trang trí nội thất",
      code: "Velvet MQ-2847",
      shop: "Nguồn sáng xanh",
      price: 50000,
      quantity: 3,
      isChecked: true,
      image: Img3,
    },
  ];
  
  const ship = 20000;
  const total = (productList) => {
    let newProList = [...productList];
    let count = 0;
    let total = 0;
    newProList.map((product) => {
      if (product.isChecked) {
        let amount = product.quantity * product.price;
        total += amount;
        count += product.quantity;
      }
    });
    return { total, count };
  };

  const [productList, setProductList] = React.useState(products);
  const [totalItems, setTotalItems] = React.useState(total(productList));
  
  const listItems = productList.map((product) => (
    <>
      <tr>
        <td>
          <div className="product-info">
            <img
              className="product-img mr-2"
              src={product.image}
              alt="product-img"
            />
            <div className="product-name">
              <a className="pro-name">{product.name}</a>
              <a className="pro-supplyer">{product.shop}</a>
            </div>
          </div>
        </td>
        <td>
          <CurrencyFormat
            value={product.price}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"đ"}
            renderText={(value) => <div>{value}</div>}
          />
        </td>
        <td>
          <div className="quantity">
            <input
              className="quantity"
              id="txt_quantity"
              min="0"
              name="form-0-quantity"
              value={product.quantity}
              type="number"
              disabled
            />
          </div>
        </td>
        <td>
          <CurrencyFormat
            value={product.price * product.quantity}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"đ"}
            renderText={(value) => <div>{value}</div>}
          />
        </td>
      </tr>
    </>
  ));

  return (
    <>
      <Header />
      <Menu />
      <Breadcumb items={items} />
      <div>
        <div class="divTiltle">
          <object type="image/svg+xml" data={Step3}>
            *
          </object>
        </div>
        <Container>
          <Table responsive="md">
            <thead>
              <tr>
                <th>Sản phẩm</th>
                <th>Đơn giá</th>
                <th>Số lượng</th>
                <th>Số tiền</th>
              </tr>
            </thead>
            <tbody>{listItems}</tbody>
            <tfoot>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </Table>
        </Container>
        <div class="total-1">
          <div class="group">
            <div className="title">Tổng tiền ({totalItems.count} sản phẩm)</div>
            <div className="total-price">
              <CurrencyFormat
                value={totalItems.total}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"đ"}
                renderText={(value) => <div>{value}</div>}
              />
            </div>
          </div>

          <div class="group">
            <div class="title">Phí vận chuyển</div>
            <div clas="total-price">
              <div className="total-price">
                <CurrencyFormat
                  value={ship}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"đ"}
                  renderText={(value) => <div>{value}</div>}
                />
              </div>
            </div>
          </div>
          <div class="group">
            <div class="title">Tổng tiền đơn hàng</div>
            <div clas="total-price">
              <div className="total-price">
                <CurrencyFormat
                  value={totalItems.total + ship}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"đ"}
                  renderText={(value) => <div>{value}</div>}
                />
              </div>
            </div>
          </div>
        </div>
        <div class="foot">
          <Button
            variant="primary"
            size="lg"
            className="btnBack"
            onClick={() => {
              window.open("http://localhost:3000/payment-methods", "_self");
            }}
          >
            <FontAwesomeIcon
              icon={faLongArrowAltRight}
              size="2x"
              rotation={180}
            />
          </Button>
          <Button
            variant="primary"
            size="lg"
            className="btnCheckout"
            onClick={handleClick}
          >
            Thanh toán đơn hàng
          </Button>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Checkout;
