import React, { useEffect } from 'react';

import systemContant from '../../config/constant';
import authentication from '../../services/authentication';

function Logout() {
  // --- Effect
  useEffect(() => {
    authentication.logout();
    window.open(systemContant.CLIENT_DOMAIN + '/login', '_self');
  }, []);

  // --- Render
  return <div></div>;
}

export default Logout;