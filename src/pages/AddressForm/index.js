import React from "react";
import { Form, Button } from "react-bootstrap";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Menu from "../../components/Menu";
import Breadcumb from "../../components/Breadcumb";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltRight } from "@fortawesome/free-solid-svg-icons";
import Step1 from "./images/proccess-bar.svg";

const AddressForm = () => {
  const items = [
    {
      text: "Trang chủ",
      link: "/",
      check: false,
    },
    {
      text: "Trang giỏ hàng",
      link: "/cart-table",
      check: false,
    },
    {
      text: "Thanh toán giỏ hàng",
      link: "#",
      check: true,
    },
  ];

  const cities = [
    {
      name: "Hồ Chí Minh",
      districts: [
        {
          name: "Quận 1",
          wards: [
            { name: "Phường Bến Nghé" },
            { name: "Phường Bến Thành" },
            { name: "Phường Cầu Kho" },
            { name: "Phường Cầu Ông Lãnh" },
            { name: "Phường Cô Giang" },
            { name: "Phường Đa Kao" },
            { name: "Phường Nguyễn Cư Trinh" },
            { name: "Phường Nguyễn Thái Bình" },
            { name: "Phường Phạm Ngũ Lão" },
            { name: "Phường Tân Định" },
          ],
        },
        { name: "Quận 2" },
        { name: "Quận 3" },
        { name: "Quận 4" },
        { name: "Quận 5" },
        { name: "Quận 6" },
        { name: "Quận 7" },
        { name: "Quận 8" },
        { name: "Quận 9" },
        { name: "Quận 10" },
        { name: "Quận 11" },
        { name: "Quận 12" },
        { name: "Quận Bình Tân" },
        { name: "Quận Bình Thạnh" },
        { name: "Quận Tân Bình" },
        { name: "Quận Tân Phú" },
        { name: "Quận Phú Nhuận" },
        { name: "Quận Gò Vấp" },
        { name: "Quận Thủ Đức" },
      ],
    },
    { name: "Hà Nội" },
    { name: "Đà Nẵng" },
    { name: "Hải Phòng" },
    { name: "Cần Thơ" },
  ];

  const districtList = cities[0].districts.map((district) => (
    <>
      <option>{district.name}</option>
    </>
  ));

  const cityList = cities.map((city) => (
    <>
      <option>{city.name}</option>
    </>
  ));

  const wardList = cities[0].districts[0].wards.map((ward) => (
    <>
      <option>{ward.name}</option>
    </>
  ));

  return (
    <>
      <Header />
      <Menu />
      <Breadcumb items={items} />

      <div class="divTiltle">
        <object type="image/svg+xml" data={Step1}>
          *
        </object>
      </div>
      <div class="divForm">
        <Form className="address-form">
          <h4>Thông tin người nhận hàng</h4>
          <Form.Group controlId="formUsername">
            <Form.Label>
              Họ tên<span>*</span>
            </Form.Label>
            <Form.Control type="text" placeholder="Nhập họ tên" />
          </Form.Group>

          <Form.Group controlId="formNumber">
            <Form.Label>
              Số điện thoại<span>*</span>
            </Form.Label>
            <Form.Control type="numbêr" placeholder="Nhập số điện thoại" />
          </Form.Group>
          <Form.Group>
            <Form.Label>
              Thành phố<span>*</span>
            </Form.Label>
            <Form.Control as="select">{cityList}</Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>
              Quận / Huyện<span>*</span>
            </Form.Label>
            <Form.Control as="select">{districtList}</Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>
              Phường / Xã<span>*</span>
            </Form.Label>
            <Form.Control as="select">{wardList}</Form.Control>
          </Form.Group>
          <Form.Group controlId="formStreet">
            <Form.Label>
              Đường<span>*</span>
            </Form.Label>
            <Form.Control type="text" placeholder="Nhập số nhà, đường" />
          </Form.Group>
        </Form>
      </div>
      <div class="foot next">
        <Button
          variant="primary"
          size="lg"
          className="btnBack"
          onClick={() => {
            window.open("http://localhost:3000/payment-methods", "_self");
          }}
        >
          <FontAwesomeIcon icon={faLongArrowAltRight} size="2x" />
        </Button>
      </div>
      <Footer />
    </>
  );
};

export default AddressForm;
