import React, { useEffect } from "react";
import { Row, Col, Select, Pagination, BackTop } from "antd";
import { ArrowUpOutlined } from "@ant-design/icons";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Menu from "../../components/Menu";
import BreadCrumb from "../../components/Breadcumb";
import CardPicture from "../../components/CardPicture";
import WOW from "wowjs";
import ReactWOW from "react-wow";
import "./Home.scss";
import pic1 from "./images/pic1.png";
import pic2 from "./images/pic2.jfif";
import pic3 from "./images/pic3.jfif";
import pic4 from "./images/pic4.jfif";
import pic5 from "./images/pic5.png";
import pic6 from "./images/pic6.jfif";
import pic7 from "./images/pic7.jfif";
import pic8 from "./images/pic8.jfif";
import ListRoom from "../../components/ListRoom";
ImageIdea.propTypes = {};

const { Option } = Select;

function ImageIdea(props) {
  useEffect(() => {
    new WOW.WOW({
      live: false,
    }).init();
  }, []);

  const cardPic = [
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic1,
    },
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic2,
    },
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic3,
    },
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic4,
    },
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic5,
    },
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic6,
    },
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic7,
    },
    {
      content: "Kiểu trang trí nội thất hiện đại",
      author: "Nguyễn Hoài Bảo",
      source: pic8,
    },
  ];
  const items = [
    {
      text: "Trang chủ",
      link: "/",
      check: false,
    },
    {
      text: "Ý tưởng",
      link: "/",
      check: false,
    },
    {
      text: "Hình ảnh",
      link: "#",
      check: true,
    },
  ];
  const [index, setIndex] = React.useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  const a = [<div>Phong Cách</div>, <div>Màu sắc</div>, <div>Kích thước</div>];
  return (
    <div>
      <Header />
      <Menu />
      <BreadCrumb items={items} />

      <div className="listCard">
        <div className="selectStyle">
          <Select placeholder={a[0]} className="subSelectStyle">
            <Option value="Hiện đại">Hiện đại</Option>
            <Option value="Cổ điển">Cổ điển</Option>
          </Select>
          <Select placeholder={a[1]} className="subSelectStyle">
            <Option value="Đỏ">Đỏ</Option>
            <Option value="Xanh">Xanh</Option>
            <Option value="Vàng">Vàng</Option>
          </Select>
          <Select placeholder={a[2]} className="subSelectStyle">
            <Option value="Nhỏ">Nhỏ</Option>
            <Option value="Trung bình">Trung bình</Option>
            <Option value="Lớn">Lớn</Option>
          </Select>
        </div>
        <div style={{ marginRight: "20px" }}>
          <ListRoom />
        </div>
        <Row gutter={24}>
          {cardPic.map((card, index) => (
            <Col span={12} className="subCard">
              <div
                className={`wow ${index % 2 === 0 ? "fadeInUp" : "fadeInUp"}`}
                data-wow-delay={index % 2 === 0 ? "0.3s" : "0.6s"}
              >
                <CardPicture
                  key={index}
                  content={card.content}
                  author={card.author}
                  source={card.source}
                />
              </div>
            </Col>
          ))}
        </Row>
        <div className="wow fadeInRight" data-wow-delay="0.5s">
          <Pagination
            style={{ float: "right" }}
            total={100}
            // showSizeChanger
            // showTotal={(total) => `Total ${total} items`}
          />
        </div>
      </div>
      <BackTop>
        <div className="btn-Top">
          <ArrowUpOutlined className="icon-Top" />
        </div>
      </BackTop>

      <Footer />
    </div>
  );
}

export default ImageIdea;
