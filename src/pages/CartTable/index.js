import React from "react";
import {
  Table,
  Form,
  Button,
  Container,
  Row,
  Col,
  Image,
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CurrencyFormat from "react-currency-format";
import { faTimes, faHome } from "@fortawesome/free-solid-svg-icons";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Menu from "../../components/Menu";
import Breadcumb from "../../components/Breadcumb";
import Img1 from "./images/Rectangle5.png";
import Img2 from "./images/ghe.png";
import Img3 from "./images/erectangle_5.png";
import EmptyCart from "./images/empty-cart.png";

const CartTable = () => {
  const products = [
    {
      id: 1,
      name: "Bồn tắm Oval",
      code: "RU-8329",
      shop: "Euro King",
      price: 1200000,
      quantity: 1,
      isChecked: true,
      image: Img1,
    },
    {
      id: 2,
      name: "Ghế phòng ăn",
      code: "Velvet MQ-2847",
      shop: "Winchar.vn",
      price: 250000,
      quantity: 2,
      isChecked: true,
      image: Img2,
    },
    {
      id: 3,
      name: "Đèn trần thả kim cương trang trí nội thất",
      code: "Velvet MQ-2847",
      shop: "Nguồn sáng xanh",
      price: 50000,
      quantity: 3,
      isChecked: true,
      image: Img3,
    },
  ];

  const total = (productList) => {
    let newProList = [...productList];
    let count = 0;
    let total = 0;
    newProList.map((product) => {
      if (product.isChecked) {
        let amount = product.quantity * product.price;
        total += amount;
        count += product.quantity;
      }
    });

    return { total, count };
  };
  const [checkAll, setCheckAll] = React.useState(false);
  const [productList, setProductList] = React.useState(products);
  const [totalItems, setTotalItems] = React.useState(total(productList));
  
  const items = [
    {
      text: "Trang chủ",
      link: "/",
      check: false,
    },
    {
      text: "Trang giỏ hàng",
      link: "/cart-table",
      check: true,
    },
  ];

  const handleAllChecked = (e) => {
    setCheckAll(e.target.checked);
    let newProList = productList;
    newProList.forEach((product) => (product.isChecked = e.target.checked));
    setProductList(newProList);
    setTotalItems(total(newProList));
  };

  const handleItemChecked = (e) => {
    let newProList = [...productList];
    newProList.forEach((product) => {
      if (+product.id === +e.target.id) {
        product.isChecked = e.target.checked;
      }
    });

    setProductList(newProList);
    setTotalItems(total(newProList));
  };

  const handleQuantityAdd = (id) => {
    let newProList = [...productList];
    newProList.forEach((product) => {
      if (+product.id === +id) {
        product.quantity += 1;
      }
    });

    setProductList(newProList);
    setTotalItems(total(newProList));
  };

  const handleQuantitySubtract = (id) => {
    let newProList = [...productList];
    newProList.forEach((product) => {
      if (+product.id === +id) {
        if (+product.quantity > 1) product.quantity -= 1;
        else {
          newProList = newProList.filter((pro) => +pro.id !== +id);
        }
      }
    });

    setProductList(newProList);
    setTotalItems(total(newProList));
  };

  const handleRemoveProduct = (id) => {
    let newProList = [...productList];
    newProList.forEach((product) => {
      if (+product.id === +id) {
        newProList = newProList.filter((pro) => +pro.id !== +id);
      }
    });

    setProductList(newProList);
    setTotalItems(total(newProList));
  };

  const listItems = productList.map((product) => (
    <>
      <tr>
        <td key={product.id}>
          <Form.Check
            type="checkbox"
            id={product.id}
            checked={product.isChecked}
            onChange={handleItemChecked}
          />
        </td>
        <td>
          <div className="product-info">
            <img
              className="product-img mr-2"
              src={product.image}
              alt="product-img"
            />
            <div className="product-name">
              <a className="pro-name">{product.name}</a>
              <a className="pro-supplyer">{product.shop}</a>
            </div>
          </div>
        </td>
        <td>
          <CurrencyFormat
            value={product.price}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"đ"}
            renderText={(value) => <div>{value}</div>}
          />
        </td>
        <td>
          <div className="quantity">
            <button
              className="btn minus1"
              onClick={() => handleQuantitySubtract(product.id)}
            >
              -
            </button>
            <input
              className="quantity"
              id="txt_quantity"
              min="0"
              name="form-0-quantity"
              value={product.quantity}
              type="number"
            />
            <button
              className="btn add1"
              productId={product.id}
              onClick={() => handleQuantityAdd(product.id)}
            >
              +
            </button>
          </div>
        </td>
        <td>
          <CurrencyFormat
            value={product.price * product.quantity}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"đ"}
            renderText={(value) => <div>{value}</div>}
          />
        </td>
        <td>
          <Button
            className="btnRemove"
            onClick={() => handleRemoveProduct(product.id)}
          >
            <FontAwesomeIcon icon={faTimes} />
          </Button>
        </td>
      </tr>
    </>
  ));

  const cart = (
    <>
      <Table responsive="md">
        <thead>
          <tr>
            <th>
              <Form.Check
                type="checkbox"
                checked={checkAll}
                onChange={handleAllChecked}
              />
            </th>
            <th>Sản phẩm</th>
            <th>Đơn giá</th>
            <th>Số lượng</th>
            <th>Số tiền</th>
            <th>Loại bỏ</th>
          </tr>
        </thead>
        <tbody>{listItems}</tbody>
      </Table>
      <div className="total">
        <div className="title">Tổng tiền ({totalItems.count} sản phẩm)</div>
        <div className="total-price">
          <CurrencyFormat
            value={totalItems.total}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"đ"}
            renderText={(value) => <div>{value}</div>}
          />
        </div>
      </div>
      <div className="checkout">
        <Button
          className="btn checkout-btn"
          onClick={() => {
            window.open("http://localhost:3000/address-form", "_self");
          }}
        >
          Thanh toán giỏ hàng
        </Button>
      </div>
    </>
  );

  const empty = (
    <>
      <div className=".empty-cart d-flex flex-column align-items-center">
        <div className="d-flex justify-content-center align-items-center">
          <Image src={EmptyCart} fluid />
        </div>
        <Button
          className="btnBackHome"
          onClick={() => {
            window.open("http://localhost:3000", "_self");
          }}
        >
          <FontAwesomeIcon icon={faHome} />
          Quay về mua sắm
        </Button>
      </div>
    </>
  );
  const body = productList.length === 0 ? empty : cart;
  return (
    <>
      <Header />
      <Menu />
      <Breadcumb items={items} />
      <Container>{body}</Container>
      <Footer />
    </>
  );
};

export default CartTable;
