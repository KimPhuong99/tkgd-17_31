import React, { useState } from 'react'
import './index.css';
import face from './images/fb.png';
import gg from './images/gg.png';
import tw from './images/tw.png';
import authentication from '../../services/authentication';
function Login() {
    const [userame, setUserName] = useState('');
    const handle = (e) => {


    }
    return (
        <div className="body-login" width="100%">
            <div className='flex-container'>
                <div className='flex-img'>

                </div>
                <div className='flex-form'>
                    <div className='logo-login'></div>
                    <input class="userame" type="text" placeholder="Tên đăng nhập" value={userame}
                        onChange={(e) => setUserName(e.target.value)} />
                    <input class="password" type="password" placeholder="Mật khẩu" />
                    <input className="check-box" type="checkbox" />
                    <label className="text-check">Nhắc nhở tôi</label>
                    <a className="text-check2 ">Quên mật khẩu?</a>
                    <button className="button"
                    onClick={() => {
                      authentication.login({
                        user: {
                          name: userame
                        }
                      });
                      window.open('http://localhost:3000/', '_self');
                    }}
                    ><span className="text3" onClick ={handle}>Đăng nhập</span></button>
                    <hr className="hr1" />
                    <hr className="hr2"></hr>
                    <img src={face} class="face"></img>
                    <img src={gg} class="gg"></img>
                    <img src={tw} class="tw"></img>
                    <a className="text6">Đăng ký</a>
                </div>
            </div>
          </div>
    )
}

export default Login