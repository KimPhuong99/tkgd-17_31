import React from "react";
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Menu from '../../components/Menu';
import Breadcumb from '../../components/Breadcumb';
import { faLongArrowAltRight } from "@fortawesome/free-solid-svg-icons";
import Step2 from "./images/proccess-bar.svg";

const PaymentMethods = () => {
  const [selectOption, setSelectOption] = React.useState('option4');
  function handleOptionChange (e){
     setSelectOption(e.target.value);
  }
  const items = [
    {
      text: 'Trang chủ',
      link: '/',
      check: false
    },
    {
      text: 'Trang giỏ hàng',
      link: '/cart-table',
      check: false
    },
    {
      text: 'Thanh toán giỏ hàng',
      link: '#',
      check: true
    }
  ];
  return (
    <>
      <Header />
      <Menu />
      <Breadcumb items={items} />
      
      <div class="divTiltle">
        <object type="image/svg+xml" data={Step2}>*</object>
      </div>
      <div class="divForm">
        <form id="frmPayment">
          <h4>Phương thức thanh toán</h4>
          <div className="radio">
            <label>
              <input
                type="radio"
                value="option1"
                checked={selectOption === "option1"}
                onChange={handleOptionChange}
              />
              <span class="mr-auto">Ví điện tử</span>
            </label>
          </div>
          <div className="radio">
            <label>
              <input
                type="radio"
                value="option2"
                checked={selectOption === "option2"}
                onChange={handleOptionChange}
              />
              <span class="mr-auto">Thẻ ghi nợ / tín dụng</span>
            </label>
          </div>
          <div className="radio">
            <label>
              <input
                type="radio"
                value="option3"
                checked={selectOption === "option3"}
                onChange={handleOptionChange}
              />
              <span class="mr-auto">Internet Banking</span>
            </label>
          </div>
          <div className="radio">
            <label>
              <input
                type="radio"
                value="option4"
                checked={selectOption === "option4"}
                onChange={handleOptionChange}
              />
              <span class="mr-auto">Thanh toán khi nhận hàng</span>
            </label>
          </div>
        </form>
      </div>
      <div class="foot">
        <Button variant="primary" size="lg" className="btnBack"
         onClick={() => {
          window.open('http://localhost:3000/address-form', '_self');
        }}>
          <FontAwesomeIcon
            icon={faLongArrowAltRight}
            size="2x"
            rotation={180}
          />
        </Button>
        <Button variant="primary" size="lg" className="btnBack"
         onClick={() => {
          window.open('http://localhost:3000/checkout', '_self');
        }}>
          <FontAwesomeIcon icon={faLongArrowAltRight} size="2x" />
        </Button>
      </div>
      <Footer />
    </>
  );
};

export default PaymentMethods;
